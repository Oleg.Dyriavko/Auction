package com.company;

import com.company.domain.Bid;
import com.company.domain.Buyer;
import com.company.domain.Product;
import com.company.domain.Seller;
import com.company.service.BidService;
import com.company.service.BuyerService;
import com.company.service.ProductService;
import com.company.service.SellerService;

import java.sql.SQLException;
import java.util.*;
import java.util.function.Predicate;

public class Menu {
    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        BidService bidService = new BidService();
        SellerService sellerService = new SellerService();
        BuyerService buyerService = new BuyerService();
        ProductService productService = new ProductService();

        while (true) {

            menu();
            String key = scanner.next();
            try {
                switch (key) {
                    case "1":
                        //Create seller
                        String name = getInfoFromUser("Please enter seller's name");
                        Seller seller = new Seller(1, name);
                        sellerService.saveSeller(name);
                        break;

                    case "2": {
                        //Create product and add seller
                        String city = getInfoFromUser("Please enter product's city");
                        String street = getInfoFromUser("Please enter product's street");
                        int number = Integer.parseInt(getInfoFromUser("Please enter product's number"));

                        List<Seller> sellers = sellerService.getAllSellers();

                        String sellerDB = getInfoFromUser("Please choose seller from list" + sellers);

                        Predicate<Seller> sellerPredicate = s -> s.getName().equals(sellerDB);

                        int sellerId = 0;

                        String info = "Enter correct seller's name";

                        if (sellers.stream().anyMatch(sellerPredicate)) {
                            sellerId = sellers.stream().filter(sellerPredicate).findFirst().get().getId();

                            productService.saveProduct(city, street, number, sellerId);

                            info = "Product with number " + number + " is saved";
                        }

                        System.out.println(info);

                        break;
                    }


                    case "3":
                        //Create buyers
                        String firstName = getInfoFromUser("Please enter buyer's firstName");

                        String lastName = getInfoFromUser("Please enter buyer's lastName");
                        buyerService.saveBuyer(firstName, lastName);
                        break;


                    case "4": {
                        //Add bids from different buyers for a product

                        int nominal = Integer.parseInt(getInfoFromUser("Please, enter bid's nominal"));

                        List<Buyer> buyers = buyerService.getAllBuyers();

                        String buyerFullName = getInfoFromUser("Please choose buyer's firstname and lastname from list" + buyers);

                        Predicate<Buyer> buyerPredicate = b -> b.getFullName().equals(buyerFullName);

                        List<Product> products = productService.getAllProducts();

                        int productNumber = Integer.parseInt(getInfoFromUser("Please choose product's number"));

                        Predicate<Product> productPredicate = b -> b.getNumber() == productNumber;


                        if (buyers.stream().anyMatch(buyerPredicate) && products.stream().anyMatch(productPredicate)) {

                            int buyerId = buyers.stream().filter(buyerPredicate).findFirst().get().getId();

                            int productId = products.stream().filter(productPredicate).findFirst().get().getId();

                            bidService.saveBid(nominal, productId, buyerId);

                            System.out.println("Bid with nominal " + nominal + " is saved");
                        } else {
                            System.out.println("Enter correct buyer's name");
                        }

                        break;
                    }


                    case "5":
                        //enter correct product number
                        int productNumber = Integer.parseInt(getInfoFromUser("Enter product number"));

                        bidService.compareBidsForProductAndCloseAuction(productNumber);
                        break;

                }
            } catch (SQLException e) {
                System.out.println("Exception: " + e);
            } catch (Exception e) {
                System.out.println("Exception: " + e);
            }
        }
    }


    private static String getInfoFromUser(String message) {
        System.out.println(message);
        return new Scanner(System.in).nextLine();
    }

    public static void menu() {
        System.out.println(
                "1) Create seller.\n" +
                        "2) Create product and add seller.\n" +
                        "3) Create 2 or more buyers.\n" +
                        "4) Add bids from different buyers for a product.\n" +
                        "5) Close auction.");
    }
}