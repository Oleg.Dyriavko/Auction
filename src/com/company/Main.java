package com.company;

import com.company.domain.Bid;
import com.company.domain.Buyer;
import com.company.domain.Product;
import com.company.domain.Seller;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;


public class Main {

    public static void main(String[] args) throws SQLException, ClassNotFoundException {
        List<Product> products = new ArrayList<>();
        List<Seller> sellers = new ArrayList<>();
        List<Buyer> buyers = new ArrayList<>();
        List<Bid> bids = new ArrayList<>();

        // write your code here

        //Class.forName("com.mysql.jdbc.Driver");

        try (Connection connection = DriverManager.getConnection(
                "jdbc:mysql://localhost:3306/auction",
                "root",
                "root")) {
            try (PreparedStatement psProduct =
                         connection.prepareStatement("SELECT * FROM product")) {
                ResultSet resultSetProduct = psProduct.executeQuery();
                while (resultSetProduct.next()) {
                    int productId = resultSetProduct.getInt("product_id");
                    String city = resultSetProduct.getString("city");
                    String street = resultSetProduct.getString("street");
                    int number = resultSetProduct.getInt("number");
                    int sellerId = resultSetProduct.getInt("seller_id");
                    products.add(new Product(productId, city, street, number, sellerId));
                }
            }

            products.forEach(e -> System.out.println(e));

            try (PreparedStatement psSeller =
                         connection.prepareStatement("SELECT * FROM seller")) {
                ResultSet resultSetSeller = psSeller.executeQuery();
                while (resultSetSeller.next()) {
                    int sellerId = resultSetSeller.getInt("seller_id");
                    String name = resultSetSeller.getString("name");
                    sellers.add(new Seller(sellerId, name));
                }
            }
            sellers.forEach(e -> System.out.println(e));

            try (PreparedStatement psByer =
                         connection.prepareStatement("SELECT *FROM buyer")) {
                ResultSet resultSetBuyer = psByer.executeQuery();
                while (resultSetBuyer.next()) {
                    int buyerId = resultSetBuyer.getInt("buyer_id");
                    String firstName = resultSetBuyer.getString("first_name");
                    String lastdName = resultSetBuyer.getString("last_name");
                    buyers.add(new Buyer(buyerId,firstName, lastdName));
                }

            }
            buyers.forEach(e -> System.out.println(e));

            try (PreparedStatement psBid = connection.prepareStatement("SELECT *FROM bid")) {
                ResultSet resultSetBid = psBid.executeQuery();
                while (resultSetBid.next()) {
                    int bidId = resultSetBid.getInt("bid_id");
                    int buyerId = resultSetBid.getInt("buyer_id");
                    int productId = resultSetBid.getInt("product_id");
                    int nominal = resultSetBid.getInt("nominal");
                    bids.add(new Bid(bidId, buyerId, productId, nominal));
                }

            }
            bids.forEach(e -> System.out.println(e));
        }

    }
}


