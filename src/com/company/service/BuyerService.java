package com.company.service;

import com.company.domain.Buyer;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class BuyerService {


    public static final String INSERT_BUYER = "INSERT INTO buyer (first_name, last_name) VALUES(?, ?)";

    public void saveBuyer(String firstName, String lastName) throws SQLException {
        try (Connection connection = DriverManager.getConnection(
                "jdbc:mysql://localhost:3306/auction",
                "root",
                "root");
             PreparedStatement insertStatement = connection.prepareStatement(INSERT_BUYER)) {
            insertStatement.setString(1, firstName);
            insertStatement.setString(2, lastName);
            insertStatement.executeUpdate();
        }
    }

    public List<Buyer> getAllBuyers() throws SQLException {
        String allBuyers = "SELECT * FROM buyer";
        List<Buyer> buyers = new ArrayList<>();
        try (Connection connection = DriverManager.getConnection(
                "jdbc:mysql://localhost:3306/auction",
                "root",
                "root");
        ) {
            ResultSet resultSet = connection.createStatement().executeQuery(allBuyers);
            while (resultSet.next()) {
                int buyerId = resultSet.getInt("buyer_id");
                String firstName = resultSet.getString("first_name");
                String lastName = resultSet.getString("last_name");
                Buyer buyer = new Buyer(buyerId, firstName, lastName);
                buyers.add(buyer);
            }
        }

        return buyers;
    }

}
