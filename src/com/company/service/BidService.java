package com.company.service;

import com.company.domain.Bid;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;


public class BidService {


    public void saveBid(int nominal, int productId, int buyerId) throws SQLException {
        String insertBid = "INSERT INTO bid (product_id, buyer_id, nominal) " +
                "VALUES(?, ?, ?)";
        try (Connection connection = DriverManager.getConnection(
                "jdbc:mysql://localhost:3306/auction",
                "root",
                "root");
             PreparedStatement insertStatement = connection.prepareStatement(insertBid)) {
            insertStatement.setInt(1, productId);
            insertStatement.setInt(2, buyerId);
            insertStatement.setInt(3, nominal);
            insertStatement.executeUpdate();
        }
    }


    public void compareBidsForProductAndCloseAuction(int productNumber) throws SQLException {
        String query = "SELECT *\n" +
                " FROM bid b \n" +
                "INNER JOIN product p\n" +
                "ON b.product_id = p.product_id\n" +
                "INNER JOIN buyer bu\n" +
                "ON bu.buyer_id = b.buyer_id\n" +
                "WHERE p.NUMBER = " + productNumber;

        List<Bid> bids = new ArrayList<>();

        String firstName = null;
        String lastName = null;
        int number = 0;

        try (Connection connection = DriverManager.getConnection(
                "jdbc:mysql://localhost:3306/auction",
                "root",
                "root")) {
            ResultSet resultSet = connection.createStatement().executeQuery(query);

            while (resultSet.next()) {
                int nominal = resultSet.getInt("nominal");
                firstName = resultSet.getString("first_name");
                lastName = resultSet.getString("last_name");
                number = resultSet.getInt("NUMBER");
                bids.add(new Bid(nominal));
            }

            int maxBid = bids.stream().mapToInt(Bid::getNominal).max().getAsInt();

            System.out.println("Buyer that win! = " + firstName + " " + lastName);
            System.out.println("Product that sold -> " + number);
            System.out.println("Auction is close");
        }
    }
}

